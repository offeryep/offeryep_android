package com.oferyep.com.adapter;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;

import com.oferyep.com.main.activity.products.ProductDetailFragment;
import com.oferyep.com.pojo_class.home_explore_pojo.ExploreResponseDatas;

import java.util.ArrayList;

/**
 * Created by embed on 20/8/18.
 */


public class ProductDetailPagerAdap extends FragmentPagerAdapter {

    private ArrayList<ExploreResponseDatas> arrayListExploreDatas;
    public Activity mActivity;
   // public ProductDetailFragment fragment;

    public ProductDetailPagerAdap(android.support.v4.app.FragmentManager fm, ArrayList<ExploreResponseDatas> arrayListExploreDatas, Activity mActivity) {
        super(fm);
        this.arrayListExploreDatas = arrayListExploreDatas;
        this.mActivity = mActivity;
    }


    @Override
    public Fragment getItem(int position) {
        final ProductDetailFragment fragment = new ProductDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("exploreData", arrayListExploreDatas.get(position));
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return arrayListExploreDatas.size();
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
    }
}
