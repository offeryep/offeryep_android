package com.oferyep.com.main.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.oferyep.com.R;
import com.oferyep.com.get_current_location.FusedLocationReceiver;
import com.oferyep.com.get_current_location.FusedLocationService;
import com.oferyep.com.utility.ApiUrl;
import com.oferyep.com.utility.CommonClass;
import com.oferyep.com.utility.OkHttp3Connection;
import com.oferyep.com.utility.RunTimePermission;
import com.oferyep.com.utility.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import io.fabric.sdk.android.Fabric;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

/**
 * <h>SplashActivity</h>
 * <p>
 *     This is launch screen i.e open first when user launch the app. It stays for
 *     3second then check if user is logged-in then go to HomeActivity or else go
 *     to Landing Screen where we have option for login or signup.
 * </p>
 * @since 3/29/2017.
 * @author 3Embed
 * @version 1.0
 */
public class SplashActivity extends AppCompatActivity
{
    private static final String TAG = SplashActivity.class.getSimpleName();
    private Activity mActivity;
    private SessionManager mSessionManager;
    private String postResultData="";
    public String lat="",lng="";
    private FusedLocationService locationService;
    private String[] permissionsArray;
    private RunTimePermission runTimePermission;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);
        mActivity=SplashActivity.this;
        mSessionManager=new SessionManager(mActivity);
        permissionsArray = new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION};
        runTimePermission = new RunTimePermission(mActivity, permissionsArray, false);

        printHashKey();

        // change status bar color
        CommonClass.statusBarColor(mActivity);

        // generating unique id from FCM
        String serialNumber = FirebaseInstanceId.getInstance().getId();
        System.out.println(TAG+" "+"serial number="+serialNumber);
        if (serialNumber!=null && !serialNumber.isEmpty())
        {
            mSessionManager.setDeviceId(serialNumber);
        }

        //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        //Displaying token on logcat
        System.out.println(TAG+" "+ "My Refreshed token: " + refreshedToken);
        if (refreshedToken!=null && !refreshedToken.isEmpty())
            mSessionManager.setPushToken(refreshedToken);

        System.out.println(TAG+"get push token="+ mSessionManager.getPushToken());

        // get post data in this activity and pass it for reducing the load in home frag where its need to be show
        postApiGetOnlyData();

        // get bundle datas if notification comes in background
        Bundle bundle =getIntent().getExtras();
        System.out.println(TAG+" "+"bundle="+bundle);

        String notificationDatas="";
        if (bundle!=null)
        {
            notificationDatas= bundle.getString("body");
            System.out.println(TAG+" "+"bundle notification datas="+notificationDatas);
        }


        // Go to notification screen if any notification msg is there else Home Page
        if (notificationDatas!=null && !notificationDatas.isEmpty())
            callNotificationClass(notificationDatas);

       // setTimerForScreen(3000);
    }

    private void postApiGetOnlyData(){
        // Call all posted api
        if (CommonClass.isNetworkAvailable(mActivity)) {
            // check is user logged in or not if its logged in then show according to location if not then show all posts.
            if (mSessionManager.getIsUserLoggedIn()) {
                if (runTimePermission.checkPermissions(permissionsArray)) {
                    getCurrentLocation();
                }else {
                    setTimerForScreen(3000);
                }
            } else {
                getGuestPosts(0);
            }
        }else {
            setTimerForScreen(3000);
        }
    }

    public void forceCrash(View view) {
        throw new RuntimeException("This is a crash");
    }

    /**
     * <h>CallNotificationClass</h>
     * <p>
     *     In this method we used to receive the bundle datas when notification comes
     *     from background. After that we used to send datas to Notification activity
     *     class.
     * </p>
     * @param notificationDatas The notification datas.
     */
    private void callNotificationClass(String notificationDatas)
    {
        if (notificationDatas!=null && !notificationDatas.isEmpty())
        {
            System.out.println(TAG+" "+"bundle="+notificationDatas);
            Intent intent = new Intent(mActivity,NotificationActivity.class);
            intent.putExtra("notificationDatas",notificationDatas);
            intent.putExtra("isFromNotification",true);
            startActivity(intent);
            finish();
        }
    }

    /**
     * <h>SetTimerForScreen</h>
     * <p>
     *     In this method we used to sleep screen for three second.
     * </p>
     */
    private void setTimerForScreen(int msec)
    {
        int TIME_OUT = msec;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent=new Intent(mActivity,HomePageActivity.class);
                intent.putExtra("postResultData",postResultData);
                startActivity(intent);
                finish();
            }
        }, TIME_OUT);
    }

    /**
     * In this method we find current location using FusedLocationApi.
     * in this we have onUpdateLocation() method in which we check if
     * its not null then We call guest user api.
     */
    private void getCurrentLocation() {
        locationService = new FusedLocationService(mActivity, new FusedLocationReceiver() {
            @Override
            public void onUpdateLocation() {
                Location currentLocation = locationService.receiveLocation();
                if (currentLocation != null) {
                    lat = String.valueOf(currentLocation.getLatitude());
                    lng = String.valueOf(currentLocation.getLongitude());

                    System.out.println(TAG + " " + "lat=" + lat + " " + "lng=" + lng);

                    if (isLocationFound(lat, lng)) {
                        mSessionManager.setCurrentLat(lat);
                        mSessionManager.setCurrentLng(lng);
                        getUserPosts(0);
                    }
                }
            }
        }
        );
    }

    private boolean isLocationFound(String lat, String lng) {
        return !(lat == null || lat.isEmpty()) && !(lng == null || lng.isEmpty());
    }

    /**
     * <h>GetGuestPosts</h>
     * <p>
     * In this method we used to call guest user api to get all posts.
     * </p>
     *
     * @param offset The page index
     */
    private void getGuestPosts(int offset) {
        if (CommonClass.isNetworkAvailable(mActivity)) {
            JSONObject requestDatas = new JSONObject();
            int limit = 20;
            offset = limit * offset;
            try {
                requestDatas.put("offset", offset);
                requestDatas.put("limit", limit);
                requestDatas.put("pushToken", mSessionManager.getPushToken());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.GET_GUEST_ALL_POSTS, OkHttp3Connection.Request_type.POST, requestDatas, new OkHttp3Connection.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String result, String user_tag) {
                    JSONObject jsonObject= null;
                    try {
                        jsonObject = new JSONObject(result);
                        String code=jsonObject.getString("code");
                        String msg=jsonObject.getString("message");
                        switch (code){
                            // success response
                            case "200":
                                postResultData = result;
                                setTimerForScreen(500);
                                break;

                            // auth token expired
                            case "401":
                                CommonClass.sessionExpired(mActivity);
                                break;

                            default:
                                setTimerForScreen(500);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(String error, String user_tag) {
                    setTimerForScreen(500);
                }
            });
        }else {
            setTimerForScreen(3000);
        }
    }

    /**
     * <h>GetUserPosts</h>
     * <p>
     * In this method we used to do call getUserPosts api. And get all posts
     * in response. Once we get all post then show that in recyclerview.
     * </p>
     *
     * @param offset The pagination
     */
    private void getUserPosts(int offset) {
        if (CommonClass.isNetworkAvailable(mActivity)) {
            JSONObject requestDatas = new JSONObject();
            int limit = 20;
            offset = limit * offset;

            try {
                requestDatas.put("offset", offset);
                requestDatas.put("limit", limit);
                requestDatas.put("token", mSessionManager.getAuthToken());
                requestDatas.put("latitude", lat);
                requestDatas.put("longitude", lng);
                requestDatas.put("pushToken", mSessionManager.getPushToken());

            } catch (JSONException e) {
                e.printStackTrace();
            }

            OkHttp3Connection.doOkHttp3Connection(TAG, ApiUrl.GET_USER_ALL_POSTS, OkHttp3Connection.Request_type.POST, requestDatas, new OkHttp3Connection.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String result, String user_tag) {
                    JSONObject jsonObject= null;
                    try {
                        jsonObject = new JSONObject(result);
                        String code=jsonObject.getString("code");
                        String msg=jsonObject.getString("message");
                        switch (code){
                            // success response
                            case "200":
                                postResultData = result;
                                setTimerForScreen(500);
                                break;

                            // auth token expired
                            case "401":
                                CommonClass.sessionExpired(mActivity);
                                break;

                            default:
                                setTimerForScreen(500);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(String error, String user_tag) {
                    setTimerForScreen(500);
                }
            });
        }
        else {
            setTimerForScreen(3000);
        }
    }
    public void printHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(this.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                //md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.d("Facebook", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e("Facebook", "printHashKey()", e);
        } catch (Exception e) {
            Log.e("Facebook", "printHashKey()", e);
        }
    }
}
