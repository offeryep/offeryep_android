package com.oferyep.com.main.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.oferyep.com.R;
import com.oferyep.com.utility.TouchImageView;


public class ImagePreviewActivity extends AppCompatActivity {

   private TouchImageView imageView;
   private RelativeLayout rL_close;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview);

        String url=getIntent().getStringExtra("url");

        imageView=(TouchImageView)findViewById(R.id.iV_preview);

        rL_close=(RelativeLayout)findViewById(R.id.rL_close);

        rL_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Glide.with(this)
                .load(url)
                .placeholder(R.drawable.default_image)
                .error(R.drawable.default_image)
                .into(imageView);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
